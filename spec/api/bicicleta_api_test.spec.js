var mongoose = require ('mongoose');

var Bicicleta = require('../../models/bicicleta');

var request = require('request');
var server = require('../../bin/www');
var base_url = "http://localhost:3000/api/bicicletas";

server;

describe('Bicicletas API', () =>{
    beforeAll(function (done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err, success){
            if(err) console.log(err);
            done();
        });
    });




    describe('GET BICICLETAS /', () => {
        it('status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                console.log(JSON.parse(body).bicicletas);
                var bici = JSON.parse(body).bicicletas;
                expect(bici.color).toBe("rojo");
                done();
            });
        });
    });

 //});

 //*

    describe("DELETE BICICLETAS /delete", () => {
        var headers = { 'content-type': 'application/json' };
        var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
        it('STATUS 200', (done) => {
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.allBicis.length).toBe(1);

                done();
            });
        });
        it('Status 204', (done)=>{
            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });    


});


/*
beforeEach(() => {
    Bicicleta.allBicis = [];
    console.log('testeando...');
});
*/

/*
beforeEach(() => { console.log('testeando...'); });





describe('POST Bicicletas /delete', () => {
    it('STATUS 200', (done) => {
        var headers = { 'content-type': 'application/json' };
        var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
        request.post({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/create',
            body: aBici
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            expect(Bicicleta.allBicis.length).toBe(1);
            done();
        });
    });
    it('STATUS 204', (done) =>{
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"id": 10}';
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
             }, function (error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
                });
        });
});
*/

/*
describe('POST Bicicletas /update', () => {
    it('STATUS 200 - creación', (done) => {
        var headers = { 'content-type': 'application/json' };
        var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
        request.post({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/create',
            body: aBici
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });

    it('Status 200 - actualización', (done) => {
        var headers = { 'content-type': 'application/json' };
        var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
        request.post({
            headers: headers,
            url: 'http://127.0.0.1:3000/api/bicicletas/10/update',
            body: aBici
         }, function (error, response, body) {
            expect(Bicicleta.allBicis.length).toBe(0);
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("amarillo");
            expect(Bicicleta.findById(10).modelo).toBe("montaña");
            done();
        });
    });
});
 */
