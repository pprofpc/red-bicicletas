var mymap = L.map('main_map').setView([-31.4140, -64.2154], 12);
var i = 0;
var marcador = [];

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.openstreetmap.org/">Openstreetmap</a>',
    maxZoom: 20,
    tileSize: 512,
    zoomOffset: -1
}).addTo(mymap);


var marcador1 = L.marker([-31.4140, -64.2154]).addTo(mymap);
marcador1.bindPopup("<b>Hola mundo!</b><br>Soy un popup en un <br/>marcador aleatorio.").openPopup();
//L.marker([-31.4340, -64.2354]).addTo(mymap);
//L.marker([-31.3940, -64.2554]).addTo(mymap);

function onMapClick(e) {
    alert("Hiciste click en el mapa en: " + e.latlng);
}

mymap.on('click', onMapClick);

$.ajax({
    dataType: "json",
    url: "/bicicletas/lista",
    success: function(result){
        console.log("Bicis->: "+result);
        result.bicicletas.forEach(function(bici){
            i++;
            marcador[i] = L.marker(bici.ubicacion, {title: bici.code});
            marcador[i].addTo(mymap);
            marcador[i].bindPopup("<b>"+bici.code+"</b><br><b>Modelo: </b>"+bici.modelo+"<br/><b>Color: </b>" + bici.color).openPopup();
            console.log("Bici "+bici.code+" agregada");
        })
    }
});