var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.method.toString = function () {
    return 'code: ' + this.code + " | color: " + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){  //cb: CallBack
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*
var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}



Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(fBiciId){
    var fBici = Bicicleta.allBicis.find(x => x.id==fBiciId);
    if(fBici)
        return fBici;
    else
        throw new Error(`No existe una bicicleta con el id ${fBiciId}` );
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length; i++)
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
}

/* 
var a = new Bicicleta(1, 'rojo', 'urbana', [-31.4340, -64.2354]);

var b = new Bicicleta(2, 'blanca', 'urbana', [-31.395746, -64.248047]);


Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;
*/
