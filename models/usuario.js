var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const Token = require('./token');
const mailer = require('../mailer/mailer');

const saltRounds = 10;/*this allows us to encrypt more securely
                        this stands for two users with the same passowrd the ecryption will be different
                        */
//regularExpresion
const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    return re.test(email);
  };

//Modelo Usuario Scehma 
var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un e-mail válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
  googleId: String,
  facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario. '});

    /*this helps to call a function before action('save')
    is executed
    */
usuarioSchema.pre('save', function(next){
    //if the password changed we will encrypt the password
    if ( this.isModified('password') ){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next(); //next helps to continue with the pending action ('save')
    });
      
    //validate the password compare the two passowrd input and saved
usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
    }



//cb stands for callback
//We add a method called resrvar
usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb){
  //instance of Reserva
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId, 
        desde: desde,   
        hasta: hasta
    });
    
    //We save the Recerva
    reserva.save(cb);
}

//we add a method to send email
//Verificar en: https://ethereal.email
usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
  const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save((err) => {
    if ( err ) { return console.log(err.message)}
    const mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Verificacion de cuenta',
      text: 'Hola,\n\n' 
      + 'Por favor, para verificar su cuenta haga click en este link: \n' 
      + 'http://localhost:3000'
      + '\/token/confirmation\/' + token.token + '\n'
    };

    mailer.sendMail(mailOptions, function(err){
      if( err ) { return console.log(err.message) } ;
      console.log('Se ha enviado un email de bienvenida a: ' + email_destination);
    });
  });
}

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) { return cb(err); }

    var mailOptions = {
      from: 'no-reply@redbicicletas.com',
      to: email_destination,
      subject: 'Reseteo de password',
      text: 'Hola,\n\n' + 'Por favor, haga click en este link para resetear su password : \n'
        + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
    };

  mailer.sendMail(mailOptions, function (err) {
    if (err) { return cb(err); }

    console.log('Se ha enviado un email para resetar la password a  : ' + email_destination + '.');
  });

  cb(null);
});
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  cb
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [
        {
          googleId: condition.id,
        },
        { email: condition.emails[0].value },
      ],
    },
    (err, result) => {
      if (result) {
        cb(err, result);
      } else {
        console.log("----- CONDITION -----");
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.userName = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("----- VALUES -----");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return cb(err, result);
        });
      }
    }
  );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
    ]
  }, (err, result) => {
    if (result) { // login
      callback(err, result);
    } else { // registro
      console.log('---------- CONDITION ----------');
      console.log(condition);
      let values = {};
      values.facebookId = condition.id;
      values.email = condition.emails[0].value;
      values.nombre = condition.displayName || 'SIN NOMBRE';
      values.verificado = true;
      values.password = crypto.randomBytes(16).toString('hex');
      console.log('---------- VALUES ----------');
      console.log(values);
      self.create(values, (err, result) => {
        if (err) { console.log(err); }
        return callback(err, result);
      });
    }
  })
};

module.exports = mongoose.model('Usuario', usuarioSchema);
