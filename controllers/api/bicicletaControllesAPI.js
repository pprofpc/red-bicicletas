var Bicicleta = require('../../models/bicicleta');
const jwt = require('jsonwebtoken');

/*
//Primer versión
exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}
*/

function validarUsuario(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({ status: "error", message: err.message, data: null });
        } else {
            req.body.userId = decoded.id;

            console.log(' jwt verify: ' + decoded);

            next();
        };
    });
};

//Segunda versión, para asincronismo
exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        console.log("Bicis desde el API: "+bicis);
        res.status(200).json({ bicicletas: bicis });
    })
};




exports.bicicleta_create = function (req, res) {
    const bici = new Bicicleta();
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicletas: bici
    });
    }

exports.bicicleta_update = function (req, res) {

    const  bici = Bicicleta.findByCode(req.params.code);
    console.log("-->Bici: ")
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicletas: bici
     });
    }


exports.bicicleta_delete = async (req, res) =>{
    await Bicicleta.remove({ code: req.body.code});

    res.status(204).send(); //204: No hay contenido en la respueta
    }
