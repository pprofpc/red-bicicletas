var Bicicleta = require('../models/bicicleta');

/*
exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}
*/
exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        console.log('Bicis desde bicicletas controller: '+bicis);
        res.render('bicicletas/index',{ bicis });
    })
};

exports.bicicletas_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        console.log("Bicis desde el controller: " + bicis);
        res.status(200).json({ bicicletas: bicis });
    })
};

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
    console.log("Agregando Bici");
    const bici = new Bicicleta();
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

//UDAPTE
exports.bicicleta_update_get = async (req, res) =>{
    const bici = await Bicicleta.findOne({code: req.params.id});
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = async (req, res) =>{
    console.log('Params: '+req.params.id);
    const bici = await Bicicleta.updateOne({code: req.params.id},req.body);
    res.redirect('/bicicletas');
}
//FIN UPDATE


exports.bicicleta_delete_post = async (req, res)=> {
    await Bicicleta.remove({ code: req.body.code});

    res.redirect('/bicicletas');
}